import os
import pprint
import re
import shutil
from os import walk
from os.path import join

import numpy

from vdclib import Dashboard, DashboardUtil, ShopDrawing

dashboard = Dashboard(os.fsencode(
    'C:\\Users\\walker.lockard\\Desktop\\TCU Dashboard'))
projectPath = os.fsencode(
    'G:\\DFW Projects\\Projects\\Active Projects\\11.2868.6 TCU Worth Hills Village Bld 4 & 5')
shopDrawingPath = join(projectPath, os.fsencode(
    '16 - Shop Drawings\\16a - Completed Shop Drawings, Spools & Reports\\1 - Shop Drawings'))


exclude = set([
    os.fsencode('Archived'),
    os.fsencode('0 - From Beck'),
    os.fsencode('4 - Sleeves, Pads, & Hangers'),
    os.fsencode('5 - Details')
])


def validate(fileName):
    try:
        splitFileName = os.path.splitext(os.fsdecode(fileName))[0].split('_')
        isJobNumber = str.isdigit(splitFileName[0]) and (
            len(splitFileName[0]) == 7)
        isBldgNumber = bool(re.match(r'^[B]?\d{1,2}$', splitFileName[1]))
        isSheetNumber = bool(
            re.match(r'^([a-zA-Z]+[0-9.-]+[a-zA-z]?)+$', splitFileName[2]))
        isSystem = []
        for system in splitFileName[3].split('-'):
            isSystem.append(
                bool(re.match(r'^(DUCT|PIPE|MECH|COND|DWVP|WATR|NGAS|CAIR|LGAS|MGAS|RISERS)$', system)))
        isSystem = all(isSystem)
        isRevNumber = str.isdigit(splitFileName[4])
        isStatus = bool(re.match(r'^(IFA|IFC|IFR)$', splitFileName[5]))
        isDate = bool(
            re.match(r'^\d{4}[\.|-](0?[1-9]|1[012])[\.|-]([012][0-9]|3[01])$', splitFileName[6]))
        return (isJobNumber and isBldgNumber and isSheetNumber and
                isSystem and isRevNumber and isStatus and isDate)
    except IndexError as ex:
        print(f'ERROR:: Issue with file: {os.fsdecode(fileName)}:: {ex}')

def listShopDrawings(shopDrawingPath):
    shopDrawings = []
    for root, dirs, files in os.walk(shopDrawingPath, topdown=True):
        dirs[:] = [d for d in dirs if d not in exclude]
        for fileName in files:
            if(validate(fileName)):
                shopDrawings.append(ShopDrawing([root, fileName]))


    return shopDrawings


def main():
    DashboardUtil.foldersetup(dashboard)
    validatedDrawings = listShopDrawings(shopDrawingPath)
    DashboardUtil.shopdrawing_foldersetup(dashboard, validatedDrawings)
    DashboardUtil.migrate_shopdrawings(dashboard, validatedDrawings)


if __name__ == '__main__':
    main()


import os


class ShopDrawing(object):

    def __init__(self, fileLocation):
        """ShopDrawing Object

        Arguments:
            fileLocation {bytes} -- [Array of validated shopdrawing files to include root and filename]
        """

        self.fileLocation = fileLocation
        self.pathString = os.path.join(fileLocation[0], fileLocation[1])
        self.setDrawingData(fileLocation)
        self.setDashboardFileName()

    def setDrawingData(self, fileLocation):
        """Sets Drawing Data from Filename

        Arguments:
            fileLocation {bytes} -- Array of validated shopdrawing file containing root and filename

        Returns:
            Array -- Array of drawing data (Job Number, Building, Sheet Number, Service, Revision, Status, and Date in YYYY.MM.DD)
        """

        fileName = fileLocation[1]
        extensionlessFileName = os.path.splitext(os.fsdecode(fileName))[0]
        drawingData = extensionlessFileName.split('_')
        self.jobNumber = drawingData[0]
        self.building = drawingData[1]
        self.sheetNumber = drawingData[2]
        self.systems = drawingData[3].split('-')
        self.revision = drawingData[4]
        self.status = drawingData[5]
        self.date = drawingData[6]

    def setDashboardFileName(self):
        systems = '-'.join(self.systems)
        self.dashboardFileName = ('%s_%s_%s_%s.pdf' % (
            self.jobNumber, self.building, self.sheetNumber, systems))

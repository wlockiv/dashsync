import errno
import os
from os.path import join

import numpy
from pdfrw import PdfReader, PdfWriter


def foldersetup(dashboard):
    """Sets up the dashboard's root content foler

    Arguments:
        dashboard {Dashboard} -- Dashboard object.
    """

    rootPath = dashboard.rootPath
    contentFolder = dashboard.contentFolder
    for name, folder in contentFolder.items():
        try:
            contentPath = join(rootPath, folder)
            os.makedirs(contentPath)
        except OSError as ex:
            if ex.errno == errno.EEXIST and os.path.isdir(contentPath):
                pass
            else:
                print('There was a critical error while trying to make ' + name)


def shopdrawing_foldersetup(dashboard, shopdrawings):
    try:
        servicetypes = []
        for drawing in shopdrawings:
            servicetypes.append(drawing.systems[0])
        unique_servicetypes = numpy.unique(ar=servicetypes, axis=0)
        for type in unique_servicetypes:
            os.makedirs(join(dashboard.getContentDir('shopDrawings'),
                            os.fsencode(type)), exist_ok=True)
    except TypeError as te:
        argtype = type(shopdrawings)
        print('Shopdrawings should be array-like, but %s was passed.' % ())


def migrate_shopdrawings(dashboard, shopdrawings):
    for drawing in shopdrawings:
        rightDir = join(dashboard.getContentDir('shopDrawings'), os.fsencode(
            drawing.systems[0]))
        rightPath = join(rightDir, os.fsencode(drawing.dashboardFileName))
        if os.path.exists(rightPath):
            rightRevision = PdfReader(rightPath).Info.revision.strip('()')
            if int(drawing.revision) > int(rightRevision):
                trailer = PdfReader(drawing.pathString)
                trailer.Info.revision = drawing.revision
                trailer.Info.status = drawing.status
                trailer.Info.date = drawing.date
                PdfWriter(rightPath, trailer=trailer).write()
                print('%s was updated from rev %s to  rev %s' % (
                    drawing.dashboardFileName, rightRevision, drawing.revision))
            elif int(drawing.revision) < int(rightRevision):
                print('WARNING: %s: The filename revision # from the project folder is less than the dashboard copy\'s!' % (
                    drawing.dashboardFileName))
        else:
            trailer = PdfReader(drawing.pathString)
            trailer.Info.revision = drawing.revision
            trailer.Info.status = drawing.status
            trailer.Info.date = drawing.date
            PdfWriter(rightPath, trailer=trailer).write()

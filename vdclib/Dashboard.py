
import os
from os.path import join


class Dashboard(object):

    contentFolder = {
        'shopDrawings': os.fsencode('00_SHOP DRAWINGS'),
        'workPackages': os.fsencode('01_WORK PACKAGES'),
        'contractDrawings': os.fsencode('02_CONTRACT DRAWINGS'),
        'productData': os.fsencode('03_PRODUCT DATA')
    }

    def __init__(self, rootPath, contentFolder=None):
        self.rootPath = rootPath
        if not contentFolder == None:
            self.contentFolder = contentFolder

    def getContentDir(self, name):
        contentDir = join(self.rootPath, self.contentFolder[name])
        return contentDir
